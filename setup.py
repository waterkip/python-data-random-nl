from setuptools import setup

setup(
    name = 'data-random-nl',
    version =' 0.001',
    description = 'Generate fake, real or dummy data for Dutch numbers',
    url = 'http://gitlab.com/waterkip/python-data-random-nl',
    author = 'Wesley Schwengle',
    author_email = 'python@opperschaap.net',
    license = 'BSD',
    packages = ['data_random'],
    zip_safe = True
)

