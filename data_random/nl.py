
__all__ = ["generate_bsn", "generate_kvk", "generate_rsin"]

import random;

def generate_bsn(start=None):
    """Generate burgerservicenumbers"""

    bsn = None
    while bsn == None:
        bsn  = __generate_number(start)
        if (bsn[0] == 0 and bsn[1] == 0):
            bsn[1] = random.randint(1,9)
        bsn = __set_last_number_on_list(bsn, True)

    return __array_to_number(bsn);

def generate_kvk(start=None):
    """Generate Chamber of Commerce numbers"""

    kvk = None
    while kvk == None:
        # Kvk nummers are 8 long thus always start with a 0
        kvk = __generate_number(0, start)
        kvk = __set_last_number_on_list(kvk)

    del kvk[0]
    return __array_to_number(kvk);

def generate_rsin(start=None):
    """Generate RSIN numbers"""

    rsin = None
    while rsin == None:
        rsin = __generate_number(start)
        rsin = __set_last_number_on_list(rsin)

    return __array_to_number(rsin)

def __generate_number(start=None, additional=None):
    """ Generate 9 random numbers and return the list"""
    numbers = [];
    if start != None:
        numbers.append(start)

    if additional != None:
        numbers.append(additional)

    while(len(numbers) < 9):
        numbers.append(random.randint(0,9))

    return numbers

def __array_to_number(numbers):
    """Returns the giving list to a single String value"""
    return "".join(map(str, numbers))

def __set_last_number_on_list(numbers, bsn=False):
    """Calculate the last digit on the list. If the last digit is more
    than 9 returns None so you can regenerate a number"""

    numbers.reverse()

    sum = 0;
    for x in range(8, 0, -1):
        sum += ((x+1) * numbers[x])

    last = sum % 11
    if bsn == False:
        last = abs(last - 11)

    # if the last number is 10, we have an invalid number
    if last > 9:
        return

    numbers.reverse()

    numbers[-1] = last
    return numbers
