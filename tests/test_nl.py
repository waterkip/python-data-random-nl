import unittest
from data_random.nl import *
from stdnum import get_cc_module

class TestStringMethods(unittest.TestCase):

    def test_bsn(self):
        mod = get_cc_module('nl', 'bsn')
        for x in range(1,50):
            self.assertTrue(mod.validate(generate_bsn()))
            self.assertTrue(mod.validate(generate_bsn(9)))

if __name__ == '__main__':
    unittest.main()

